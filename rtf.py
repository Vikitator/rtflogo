import drawsvg as draw

from letters import letters

# User parameters

text = """
Ready   
  To Fog
""".strip('\n').split('\n')

rows = len(text)
cols = max(map(len, text))

pxwidth = 8
pxheight = 10
pxgap = 1

lettergaph = 15
lettergapv = 25

fieldgapn = 25
fieldgapw = 70
fieldgaps = 30
fieldgape = 80

borderwidth = 17
borderrad = 12

color = "black"
bgcolor = "#c7ff00"

fname = "rtf.svg"

# SVG generation

letterpxwidth = 5
letterpxheight = 7

letterwidth = letterpxwidth * (pxwidth + pxgap) - pxgap
letterheight = letterpxheight * (pxheight + pxgap) - pxgap

width = 2 * borderwidth + fieldgapw + fieldgape + cols * (letterwidth + lettergaph) - lettergaph
height = 2 * borderwidth + fieldgapn + fieldgaps + rows * (letterheight + lettergapv) - lettergapv

d = draw.Drawing(width, height)
d.append(draw.Rectangle(0.5 * borderwidth, 0.5 * borderwidth,
                        width - borderwidth, height - borderwidth, fill=bgcolor,
                        stroke=color, stroke_width=borderwidth,
                        rx=borderrad, ry=borderrad))

def pixel(x, y):
    return draw.Rectangle(x, y, pxwidth, pxheight, fill=color)

def drawletter(x, y, l):
    px_rows = letters[l].split("\n")
    px_row_anc = [x, y]
    for px_row in px_rows:
        px_cur_pos = px_row_anc.copy()
        for px in px_row:
            if px == "X":
                d.append(pixel(*px_cur_pos))
            px_cur_pos[0] += pxwidth + pxgap
        px_row_anc[1] += pxheight + pxgap

row_anc = [fieldgapw + borderwidth, fieldgapn + borderwidth]
for row in text:
    cur_pos = row_anc.copy()
    for l in row:
        drawletter(*cur_pos, l)
        cur_pos[0] += letterwidth + lettergaph
    row_anc [1] += letterheight + lettergapv

d.save_svg(fname)
