_raw = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789"
_keys = []
for i in range(8):
    _keys += list(_raw[i::8])
_keys += [" "]

_vals = """
.XXX.
X...X
X...X
XXXXX
X...X
X...X
X...X

XXXXX
X....
X....
XXXX.
X....
X....
XXXXX

XXXXX
..X. 
..X..
..X..
..X..
..X..
XXXXX

X...X
XX.XX
X.X.X
X...X
X...X
X...X
X...X

.XXX.
X...X
X...X
X...X
X.X.X
X..X.
.XX.X

X...X
X...X
X...X
X...X
X...X
X...X
.XXX.

X...X
X...X
X...X
.X.X.
..X..
..X..
..X..

...X.
..XX.
.X.X.
X..X.
XXXXX
...X.
...X.

.....
.....
.XXX.
....X
.XXXX
X...X
.XXXX

.....
.....
.XXX.
X...X
XXXXX
X....
.XXX.

.....
..X..
.....
.XX..
..X..
..X..
XXXXX

.....
.....
XXXX.
X.X.X
X.X.X
X.X.X
X.X.X

.....
.XXXX
X...X
X...X
.XXXX
....X
....X

.....
.....
X...X
X...X
X...X
X...X
.XXXX

.....
.....
X...X
X...X
.XXXX
....X
XXXX.

XXXXX
X....
x....
XXXX.
....X
X...X
.XXX.

XXXX.
X...X
X...X
XXXX.
X...X
X...X
XXXX.

XXXXX
X....
X....
XXXX.
X....
X....
X....

XXXXX
....X
....X
....X
....X
X...X
.XXX.

X...X
XX..X
X.X.X
X..XX
X...X
X...X
X...X

XXXX.
X...X
X...X
XXXX.
X.X..
X..X.
X...X

X...X
X...X
X...X
X...X
X...X
.X.X.
..X..

XXXXX
....X
...X.
..X..
.X...
X....
XXXXX

.XXX.
X...,
X....
XXXX.
X...X
X...X
.XXX.

X....
X....
X.XX.
XX..X
X...X
X...X
XXXX.

..XXX
.X...
XXXX.
.X...
.X...
.X...
.X...

....X
.....
....X
....X
....X
X...X
.XXX.

.....
.....
X.XX.
XX..X
X...X
X...X
X...X

.....
.....
X.XX.
XX..X
X....
X....
X....

.....
.....
X...X
X...X
X...X
.X.X.
..X..

.....
.....
XXXXX
...X.
..X..
.X...
XXXXX

XXXXX
....X
...X.
..X..
..X..
..X..
..X..

.XXX.
X...X
X....
X....
X....
X...X
.XXX.

.XXX.
X...X
X....
X..XX
X...X
X...X
.XXX.

X...X
X..X.
X.X..
XX...
X.X..
X..X.
X...X

.XXX.
X...X
X...X
X...X
X...X
X...X
.XXX.

.XXX.
X...X
X....
.XXX.
....X
X...X
.XXX.

X...X
X...X
X...X
X...X
X.X.X
X.X.X
.X.X.

.XXX.
X...X
X..XX
X.X.X
XX..X
X...X
.XXX.

.XXX.
X...X
X...X
.XXX.
X...X
X...X
.XXX.

.....
.....
.XXX.
X...X
X....
X...X
.XXX.

.....
.XXXX
X...X
X...X
.XXXX
....X
.XXX.

X....
X....
X...X
X..X.
XXX..
X..X.
X...X

.....
.....
.XXX.
X...X
X...X
X...X
.XXX.

.....
.....
.XXX.
X....
.XXX.
....X
.XXX.

.....
.....
X...X
X...X
X.X.X
X.X.X
.X.X.

..X..
.XX..
X.X..
..X..
..X..
..X..
XXXXX

.XXX.
X...X
X...X
.XXXX
....X
....X
.XXX.

XXXX.
X...X
X...X
X...X
X...X
X...X
XXXX.

X...X
X...X
X...X
XXXXX
X...X
X...X
X...X

X....
X....
X....
X....
X....
X....
XXXXX

XXXX.
X...X
X...X
XXXX.
X....
X....
X....

XXXXX
..X..
..X..
..X..
..X..
..X..
..X..

X...X
X...X
.X.X.
..X..
.X.X.
X...X
X...X

.XXX.
X...X
....X
...X.
..X..
.X...
XXXXX

....X
....X
.XX.X
X..XX
X...X
X...X
.XXXX

X....
X....
X.XX.
XX..X
X...X
X...X
X...X

X....
X....
X....
X....
X....
X...X
.XXX.

.....
X.XX.
XX..X
X...X
XXXX.
X....
X....

.X...
.X...
XXXX.
.X...
.X...
.X...
..XXX

.....
.....
X...X
.X.X.
..X..
.X.X.
X...X

.XXX.
X...X
....X
...X.
....X
X...X
.XXX.

.....
.....
.....
.....
.....
.....
.....
""".strip().split('\n\n')

letters = dict(zip(_keys,_vals))
